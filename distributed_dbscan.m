function [ message_cnt, labels ] = distributed_dbscan(data, eps, MinPts, num_of_loops)
%% Node start to extend spontanously. 
% if node from the same cluster meets, cluster should be merge by
% re-flooding. Code logic is the same as dbscan_distribute
message_cnt=0;

%%%%%%%%%%%%%%%%%% Init World State %%%%%%%%%%%%%%%%%% 
% every calculation and message send cost 1 cycle
all=size(data,1);

% D=zeros(all, all); % Distance between nodes
for i=1:1:all
%     for j=1:1:all
%         D(i,j)=norm(data(i,:)-data(j,:));
%     end
    nodes(i)=IoTNode();
    nodes(i).id=i;
    nodes(i).state=0;
    nodes(i).distance=data(i,:);    
    nodes(i).neighbors=[];
    nodes(i).next_neighbor=0;
    nodes(i).label=i; % label is my unique id!
end

%%%%%%%%%%%%%%%%%% Update World %%%%%%%%%%%%%%%%%% 
for loops=1:1:num_of_loops
    [cnt, nodes] = update_world(nodes, eps, MinPts);
    message_cnt = message_cnt+cnt;
end
labels=[];
for i=1:1:numel(nodes)
    labels=[labels, nodes(i).label];
end

%%%%%%%%%%%%%%%%%% F-measure %%%%%%%%%%%%%%%%%% 


end

function [ message_cnt, nodes ] = update_world( nodes, eps, MinPts )
%% main method of naive distribute dbscan
% message_cnt: counting the message passing action
% new_label: next new label id used for cluster label
% IoTNode.label is used as a cluster label

message_cnt=0;

for i=1:1:numel(nodes)    
    
    if nodes(i).next_neighbor==1+numel(nodes(i).neighbors) && numel(nodes(i).msg)==0 
        % all the neighbors are visited
        continue;
    end
    
    if numel(nodes(i).neighbors)==0 && nodes(i).next_neighbor==0
        % initialize the neighbor storage
        nodes(i).neighbors = regionQuery(i, nodes(i).distance, eps);
        nodes(i).next_neighbor=1;
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       FREE node
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if nodes(i).state==0
        
        if numel(nodes(i).neighbors)<MinPts
            nodes(i).next_neighbor=1+numel(nodes(i).neighbors);
            continue; % node is not a kernal node, stop
        end
        
        if rand(1)<0.5 % become father node by chance
            continue;
        end        
        if nodes(i).next_neighbor==0 % if not start
            nodes(i).next_neighbor=1;
        end        
        nodes(i).state=1; % start spread           
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %       PROPOGATION node
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    else % state==1
        while nodes(i).next_neighbor<=numel(nodes(i).neighbors)            
            % the next neighbor
            idx=nodes(i).next_neighbor;
            if nodes(nodes(i).neighbors(idx)).state==1 
                % the neighbor node is a marked node
                % always use the smallest label
                ok = nodes(nodes(i).neighbors(idx)).label-nodes(i).label;                
                if ok<0
                    nodes(i).label = nodes(nodes(i).neighbors(idx)).label;
                    nodes(i).next_neighbor=1;                    
                    message_cnt=message_cnt+1;
                    break;
                elseif ok>0
                    nodes(nodes(i).neighbors(idx)).label=nodes(i).label;
                    nodes(nodes(i).neighbors(idx)).next_neighbor=1;                    
                    message_cnt=message_cnt+1;
                else
                    message_cnt=message_cnt+1;
                end
            else
                % node is a FREE node
                nodes(nodes(i).neighbors(idx)).label=nodes(i).label;
                nodes(nodes(i).neighbors(idx)).state=1;
                message_cnt=message_cnt+1;
            end
            nodes(i).next_neighbor=idx+1;            
            if rand(1)<0.5 % stop one node by chance
                break;
            end            
        end
    end
end

end

function [ neighbor ] = regionQuery(self, dist, eps)
%% regionQuery get the neighbors who's distance is beneath eps
neighbor=[];
 for i=1:1:numel(dist)
    if i~=self && dist(i)<eps
        neighbor=union(neighbor,i);
    end
end
end


