function test_fmeasure(series)
%% Test 1: Features & F-measure 

moteid=1:1:size(series,1);
beta=1; % f-measure
num_of_loops=20; % max width of network
range=1:1:5; % F-measure accuracy with different network scale

for num=range
    % Train Optimal Metric
    sample = series(randsample(moteid,num),:);
    [opt_dist, emd_dist, fft_dist, diff_dist, opt_v, optL] = features_distance(sample, 0); % compute optimal for comparison
    [opt_lambda, minv] = distributed_optimization(emd_dist, fft_dist, diff_dist, sample);
    
    realnode = [sample; series(randsample(moteid,2*num),:)];
    % F-measure
    oracle=zeros(size(realnode,2)*size(realnode,1),1); 
    % oracle label, THE TRUTH, shinjitsiwa, itstomohitotsu
    for j=1:1:size(realnode,2)
        for i=1:1:size(realnode,1)
            oracle((j-1)*size(realnode,1)+i)=j;
        end
    end    
    [zopt, allemd, allfft, alldiff, zopt_v, zoptL]=features_distance(realnode,1); % need not to compute the optimal
    [dist, eps, minpts] = distributed_parameter_selection(allemd, allfft, alldiff, opt_lambda, minv, realnode);
    [message_cnt, labels] = distributed_dbscan(dist, eps, minpts, num_of_loops);
    [ratio(1,1),ratio(1,2),ratio(1,3)] = fmeasure(labels,oracle,beta);
    [dist, eps, minpts] = distributed_parameter_selection(allemd, allfft, alldiff, optL, opt_v, realnode);
    [message_cnt, labels] = distributed_dbscan(dist, eps, minpts, num_of_loops);
    [ratio(2,1),ratio(2,2),ratio(2,3)] = fmeasure(labels,oracle,beta);
    [message_cnt, labels] = distributed_dbscan(allemd, eps, minpts, num_of_loops);
    [ratio(3,1),ratio(3,2),ratio(3,3)] = fmeasure(labels,oracle,beta);
    [message_cnt, labels] = distributed_dbscan(allfft, eps, minpts, num_of_loops);
    [ratio(4,1),ratio(4,2),ratio(4,3)] = fmeasure(labels,oracle,beta);
    [message_cnt, labels] = distributed_dbscan(alldiff, eps, minpts, num_of_loops);
    [ratio(5,1),ratio(5,2),ratio(5,3)] = fmeasure(labels,oracle,beta);
    rate(:,:,num) = ratio;
    ratio
    disp(['Range ', num2str(num), ' complete.']);
%     rate(:,:,num) = optimization_distribute(emd_dist, fft_dist, diff_dist, sample, realnode, beta, -1, -1);% Set epsilon==-1    
end

f1(:,1,1)=rate(1,1,:);
recall(:,1,1)=rate(1,2,:);
precision(:,1,1)=rate(1,3,:);

% Precison advantage
p_emd(:,1,1)=rate(3,1,:);
p_fft(:,1,1)=rate(4,1,:);
p_diff(:,1,1)=rate(5,1,:);
figure(1);
plot(range, precision,':rs',range,p_emd,'-.b*',range, p_fft,'-.bo',range, p_diff,'-.bd');
axis([range(1) range(end) 0.2 1]);
title('Accuracy of Optimal Selection');
xlabel('Number of nodes');
ylabel('%');
legend('Distributed-opt','EMD','FFT','DIFF','Location','SouthEast');
set(gca, 'xticklabel', range*4*3); 
set(gca, 'yticklabel', 20:10:100);
% each mote has 4 properties, 2 times of trainning nodes needed for final test

% Recall impact
r_emd(:,1,1)=rate(3,2,:);
r_fft(:,1,1)=rate(4,2,:);
r_diff(:,1,1)=rate(5,2,:);
figure(2);
plot(range, recall,':rs',range,r_emd,'-.b*',range, r_fft,'-.bo',range, r_diff,'-.bd');
axis([range(1) range(end) 0.2 1]);
title('Recall of Optimal Selection');
xlabel('Number of nodes');
ylabel('%');
legend('Distributed-opt','EMD','FFT','DIFF','Location','SouthEast');
set(gca, 'xticklabel', range*4*3); 
set(gca, 'yticklabel', 20:10:100);

% F-measure of different network scale
figure(3);
plot(range, f1,'--rs', range, precision,'-.bo', range, recall,'-.go');
axis([range(1) range(end) 0.5 1]);
title('Impact of Network Scale');
xlabel('Number of nodes');
ylabel('%');
legend('F-measure','Accuracy','Recall','Location','SouthEast');
set(gca, 'xticklabel', range*4*3); 
set(gca, 'yticklabel', 20:10:100);


% F-measure comparasion of opt, emd, fft and diff
ave_rate=zeros(size(rate,1),size(rate,2));
for num=range
    ave_rate=ave_rate+rate(:,:,num);
end
ave_rate=ave_rate/numel(range);

figure(4);
bar(1:1:5, ave_rate);
title('Overall Comparison');
mylabels={'LP-opt', 'Distributed-opt', 'EMD', 'FFT', 'DIFF'};
set(gca,'xticklabel',mylabels);
ylabel('%');
legend('F-measure','Recall','Precision','Location','SouthEast');

end