classdef IoTNode
%% IOTNODES
    properties
        id;
        state; % state==0 FREE; state==-1 SON; state==1 FATHER
        father; % Father pointer
        label; % Used by dbscan_distribute_naive method
        msg=[]; % Mail box, for back track
        distance=[];
        neighbors; % all the neighbor within epsilon
        vis; % record next neighbor node, vis[i]=1 means i is visited
        next_neighbor;
        lambda;
    end
end

