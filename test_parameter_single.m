function main_parameter_test(series)
%% Test 2: Epsilon & F-measure
beta=1;
my_epsilon=0.2:-0.02:0.02;
my_minPts=1:1:5;
number_of_sampling=4;
num_of_loops=20;
moteid=1:1:size(series,1);

sample=series(randsample(moteid,number_of_sampling),:);
realnode=[sample; series(randsample(moteid,3*number_of_sampling),:)];    

oracle=zeros(size(realnode,2)*size(realnode,1),1); 

% Oracle label, THE TRUTH, shinjitsiwa, itstomohitotsu
for j=1:1:size(realnode,2)
    for i=1:1:size(realnode,1)
        oracle((j-1)*size(realnode,1)+i)=j;
    end
end

% Prepare Dataset
[opt_dist, emd_dist, fft_dist, diff_dist, opt_v, optL] = features_distance(sample, 1);
[zopt, allemd, allfft, alldiff, zopt_v, zoptL]=features_distance(realnode,1);

% Optimize Parameter
[opt_lambda, minv] = distributed_optimization(emd_dist, fft_dist, diff_dist, sample);
[dist, opt_eps, opt_minpts] = distributed_parameter_selection(allemd, allfft, alldiff, opt_lambda, minv, realnode);
disp(['Optimal eps = ', num2str(opt_eps), ' minpts = ', num2str(opt_minpts)]);

% Cluster Dataset and F-measure
[message_cnt, labels] = distributed_dbscan(dist, opt_eps, opt_minpts, num_of_loops);
[ratio(1,1),ratio(1,2),ratio(1,3)] = fmeasure(labels,oracle,beta);
disp(['Optimal Fmeasure = ', num2str(ratio(1,1)),' P = ', num2str(ratio(1,2)),' R = ', num2str(ratio(1,3))]);

for idx=1:1:numel(my_epsilon);
    epsilon=my_epsilon(idx);    
    for minpts=1:1:numel(my_minPts)              
        [dist, zeps, zminpts] = distributed_parameter_selection(allemd, allfft, alldiff, opt_lambda, minv, realnode);
        [message_cnt, labels] = distributed_dbscan(dist, epsilon, minpts, num_of_loops);
        [ratio(1,1),ratio(1,2),ratio(1,3)] = fmeasure(labels,oracle,beta);
        rate(:,:,idx,minpts)=[ratio(1,1),ratio(1,2),ratio(1,3)];
        disp(['Eps = ', num2str(epsilon), '  MinPts = ', num2str(minpts)]);        
    end
end
fix_f1(:,:,1,1)=rate(1,1,:,:);

% Plot surface
figure(5);
surf(fix_f1);
xlabel('MinPts');
ylabel('eps');
zlabel('%');
hold on;
m_x=opt_minpts;
m_y=max(1,floor((0.2-opt_eps)/0.02));
m_z=fix_f1(m_y,m_x);
plot3(m_x, m_y, m_z, 'o','markerfacecolor','b','markersize',10);
ytick = 1:1:10;
yticklabels = 0.2:-0.02:0.02;
set(gca, 'YTick', ytick);
set(gca, 'YTickLabel', yticklabels);
title('Parameter Selection');

end