function [ opt_lambda, minv ] = distributed_optimization( emd_dist, fft_dist, diff_dist, sample )
%% Distributed Metric Optimization Process
% Input:
% 	emd_dist, fft_dist, diff_dist: Different metric functions to be optimizd
%   sample: Raw timeseries dataset.
% Output:
%   opt_lambda: Optimal weight parameter of different metrics.

data(:,:,1)=emd_dist;
data(:,:,2)=fft_dist;
data(:,:,3)=diff_dist;

%%%%%%%%%%%%%%%%%% Init World State %%%%%%%%%%%%%%%%%% 
for i=1:1:size(data,2)
    nodes(i)=IoTNode();
    nodes(i).id=i;
    nodes(i).state=0;
    
    for j=1:1:size(data,3)
        nodes(i).distance(j,:)=data(i,:,j);
    end
    
    nodes(i).neighbors=[];
    nodes(i).next_neighbor=0;    
    nodes(i).lambda=[1/3,1/3,1/3]; % init parameter
end

for i=1:1:size(sample,2)
    for j=1:1:size(sample,1)
        nodes((i-1)*size(sample,1)+j).label=i;
    end
end

%%%%%%%%%%%%%%%%%% Update World State %%%%%%%%%%%%%%%%%% 
delta=0.01; % step length
[nodes, minv]=update_world(nodes, delta);
opt_lambda=nodes.lambda;% output optimal parameter

end

function [nodes, minv]=update_world(nodes, delta)
%% simple simulation
loops=0;
max_loop=300;

% Step 1: Select a min v
[from,minv,nodes]=select_minv(nodes);

while loops < max_loop
    loops=loops+1;
    % Step 2: Optimization
    L=nodes(from).lambda;
    ok=1;
    for a1=1:1:numel(L)
        if ok==0
            break;
        end
        for a2=a1+1:1:numel(L)
            if L(a1)>0 & L(a2)>0
                
                oldL=L;
                L(a1)=L(a1)+delta;
                L(a2)=L(a2)-delta;
                
                if L(a1)>0 & L(a2)>0                    
                    for i=1:1:numel(nodes)                        
                        nodes(i).lambda=L;
                    end
                    [from1, minv1, nodes]=select_minv(nodes);
                    if minv1>minv                        
                        from=from1;
                        minv=minv1;
                        ok=0;
                        break;
                    end
                end
                
                for i=1:1:numel(nodes)
                    nodes(i).lambda=oldL;
                end                
                L(a1)=L(a1)-2*delta;
                L(a2)=L(a2)+2*delta;
                
                if L(a1)>0 & L(a2)>0
                    for i=1:1:numel(nodes)
                        nodes(i).lambda=L;
                    end
                    [from1, minv1, nodes]=select_minv(nodes);
                    if minv1>minv                        
                        from=from1;
                        minv=minv1;
                        ok=0;
                        break;
                    end
                end
                L(a1)=L(a1)+delta;
                L(a2)=L(a2)-delta;
                for i=1:1:numel(nodes)
                    nodes(i).lambda=oldL;
                end   
                
            elseif L(a1)>0
                
                oldL=L;
                L(a1)=L(a1)-delta;
                L(a2)=L(a2)+delta;                
                if L(a1)>0 & L(a2)>0
                    for i=1:1:numel(nodes)
                        nodes(i).lambda=L;
                    end
                    [from1, minv1, nodes]=select_minv(nodes);
                    if minv1>minv                        
                        from=from1;
                        minv=minv1;
                        ok=0;
                        break;
                    end
                end
                L(a1)=L(a1)+delta;
                L(a2)=L(a2)-delta;
                for i=1:1:numel(nodes)
                    nodes(i).lambda=oldL;
                end
                
            elseif L(a2)>0
                
                oldL=L;
                
                L(a1)=L(a1)+delta;
                L(a2)=L(a2)-delta;                
                if L(a1)>0 & L(a2)>0
                    for i=1:1:numel(nodes)
                        nodes(i).lambda=L;
                    end
                    [from1, minv1, nodes]=select_minv(nodes);
                    if minv1>minv
                        from=from1;
                        minv=minv1;
                        ok=0;
                        break;
                    end
                end
                L(a1)=L(a1)-delta;
                L(a2)=L(a2)+delta;
                
                for i=1:1:numel(nodes)
                    nodes(i).lambda=oldL;
                end
            end
                        
        end
    end
    if ok==1
        break;
    end    
end

end

function [from, minv, nodes]=select_minv(nodes)
%% Find the "Support Vector"
from=0;
minv=inf;
for i=1:1:size(nodes,2)        
    dist=nodes(i).distance(:,:);        
    D=(nodes(i).lambda)*dist;

    for j=1:1:numel(D)
        if nodes(j).label~=nodes(i).label & D(j)<minv
            from=i;
            minv=D(j);                
        end
    end
    
end
end