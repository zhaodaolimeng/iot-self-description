function [ratio, P, R] = fmeasure(labels, oracle, beta)
%% Implemention of F-measure
% oracle: training label
% labels: result
% beta: 1

% % test samples
% labels=[1,1,1,2,3,2,3,3,4];
% oracle=[1,1,1,2,2,2,3,3,3];
% beta=1;

P=0; % Precision
R=0; % Recall

vis=ones(numel(labels),1);
while sum(vis)~=0
    target=oracle(vis==1);
    vis(oracle==target(1))=0;
    t=labels(oracle==target(1));
    P=P+numel(t(t==mode(t)));
end
P=P/numel(labels);

vis=ones(numel(labels),1);
while sum(vis)~=0
    target=labels(vis==1);
    vis(labels==target(1))=0;    
    t=oracle(labels==target(1));
    R=R+numel(t(t==mode(t)));
end
R=R/numel(labels);

ratio = P*R*(1+beta^2)/(P*beta^2+R);

end