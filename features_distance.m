function [opt_dist, emd_dist, fft_dist, diff_dist, v, L]=features_distance(series, dontcompute)
%% generate time series from different node plus different data types
% FIXME!!! 
% When dontcompute==1, function will not compute the final optimal result

all=[];
for j=1:1:size(series,2)
    for i=1:1:numel(series(:,1))    
        all=[all, series(i,j).Data];
    end
end

emd=zeros(size(all,2),size(all,2));
fft=zeros(size(all,2),size(all,2));
diff_fft=zeros(size(all,2),size(all,2));

for i=1:1:size(all,2)
    for j=i+1:1:size(all,2)
        
        emd(i,j)=measures(all(:,i),all(:,j),'emd');
        fft(i,j)=measures(all(:,i),all(:,j),'fft');
        diff_fft(i,j)=measures(all(:,i),all(:,j),'diff-fft');
        
        emd(j,i)=emd(i,j);
        fft(j,i)=fft(i,j);
        diff_fft(j,i)=diff_fft(i,j);
    end
end

emd_dist=(emd-min(min(emd)))./(max(max(emd))-min(min(emd)));
fft_dist=(fft-min(min(fft)))./(max(max(fft))-min(min(fft)));
diff_dist=(diff_fft-min(min(diff_fft)))./(max(max(diff_fft))-min(min(diff_fft)));

if dontcompute==1 % THIS IS ANNOYING...orz
    opt_dist=0;
    v=0;
    L=0;
    return
end

%%%%%%%%%%%%%%%%%% Compute the optimal by LP %%%%%%%%%%%%%%%%%%
A1=[];
A2=[];
A3=[];
for i=1:1:size(series,2)
    for j=1:1:size(series,1)
        for p=i+1:1:size(series,2)
            for q=1:1:size(series,1)
                A1=[A1, emd_dist((i-1)*size(series,1)+j,(p-1)*size(series,1)+q)];
                A2=[A2, fft_dist((i-1)*size(series,1)+j,(p-1)*size(series,1)+q)];
                A3=[A3, diff_dist((i-1)*size(series,1)+j,(p-1)*size(series,1)+q)];
            end
        end
    end
end

A=[A1;A2;A3;-ones(1,size(A1,2))];
A=A';

% calculate the parameters for linear programming
f=[0,0,0,-1]; % 0*lambda1+0*lambda2+0*lambda3+1*v
b=zeros(size(A,1),1); % 
A=-A;
Aeq=[1,1,1,0];
beq=1;
lb=[0,0,0,0];
ub=[];
X0=[];

options = optimset('Display','none');
x = linprog(f,A,b,Aeq,beq,lb,ub, X0, options);

% normalize distance

opt_dist=emd_dist.*x(1)+fft_dist.*x(2)+diff_dist.*x(3);
v=x(end);
L=x;

end


function [ D ] = measures( X, Y, metric )
%% main method
% usage: measures(t1.Data, t2.Data, 'emd');
%

switch metric
    case 'emd'        
        % Histograms
        nbins = 10;
        f1 = hist(X, nbins);
        f2 = hist(Y, nbins);        
        % Weights
        w1 = f1 / sum(f1);
        w2 = f1 / sum(f1);
        % Earth Mover's Distance
        [f, D] = emd(f1', f2', w1', w2', @gdf);
        % D = emd(X-Y);
    case 'fft'
        D = norm(fft(X)-fft(Y));
    case 'diff-fft'
        if size(X,1)>=2 || size(Y,1)>=2
            x=X(2:end)-X(1:end-1);
            y=Y(2:end)-Y(1:end-1);
        end
        D = norm(fft(x)-fft(y));
    otherwise
        error(['pdist2 - unknown metric: ' metric]);
end

end

function [x, fval] = emd(F1, F2, W1, W2, Func)
%%
% EMD   Earth Mover's Distance between two signatures
%    [X, FVAL] = EMD(F1, F2, W1, W2, FUNC) is the Earth Mover's Distance
%    between two signatures S1 = {F1, W1} and S2 = {F2, W2}. F1 and F2
%    consists of feature vectors which describe S1 and S2, respectively.
%    Weights of these features are stored in W1 and W2. FUNC is a function
%    which computes the ground distance between two feature vectors.
%
%    Example:
%    -------
%        f1 = [[100, 40, 22]; [211, 20, 2]; [32, 190, 150]; [2, 100, 100]];
%        f2 = [[0, 0, 0]; [50, 100, 80]; [255, 255, 255]];
%        w1 = [0.4; 0.3; 0.2; 0.1];
%        w2 = [0.5; 0.3; 0.2];
%        ...
%        [x fval] = emd(f1, f2, w1, w2, @gdf);
%        ...
%
%    EMD is formalized as linear programming problem in which the flow that
%    minimizes an overall cost function subject to a set of constraints is
%    computed. This implementation is based on "The Earth Mover's Distance
%    as a Metric for Image Retrieval", Y. Rubner, C. Tomasi and L. Guibas,
%    International Journal of Computer Vision, 40(2), pp. 99-121, 2000.
%
%    The outcome of EMD is the flow (X) which minimizes the cost function
%    and the value (FVAL) of this flow.
%
%    This file and its content belong to Ulas Yilmaz.
%    You are welcome to use it for non-commercial purposes, such as
%    student projects, research and personal interest. However,
%    you are not allowed to use it for commercial purposes, without
%    an explicit written and signed license agreement with Ulas Yilmaz.
%    Berlin University of Technology, Germany 2006.
%    http://www.cv.tu-berlin.de/~ulas/RaRF
%

% ground distance matrix
f = gdm(F1, F2, Func);

% number of feature vectors
[m a] = size(F1);
[n a] = size(F2);

% inequality constraints
A1 = zeros(m, m * n);
A2 = zeros(n, m * n);
for i = 1:m
    for j = 1:n
        k = j + (i - 1) * n;
        A1(i, k) = 1;
        A2(j, k) = 1;
    end
end
A = [A1; A2];
b = [W1; W2];

% equality constraints
Aeq = ones(m + n, m * n);
beq = ones(m + n, 1) * min(sum(W1), sum(W2));

% lower bound
lb = zeros(1, m * n);

ub = [];
X0 = [];
options = optimset('Display','none');

% linear programming
[x, fval] = linprog(f, A, b, Aeq, beq, lb, ub, X0, options);
fval = fval / sum(x);

end

function [f] = gdm(F1, F2, Func)
%%
% GDM   Ground distance matrix between two signatures
%    [F] = GDM(F1, F2, FUNC) is the ground distance matrix between
%    two signatures whose feature vectors are given in F1 and F2.
%    FUNC is a function which computes the ground distance between
%    two feature vectors.
%
%    Example:
%    -------
%        f1 = [[100, 40, 22]; [211, 20, 2]; [32, 190, 150]; [2, 100, 100]];
%        f2 = [[0, 0, 0]; [50, 100, 80]; [255, 255, 255]];
%        ...
%        [f] = gdm(f1, f2, @gmf);
%        ...
%
%    This file and its content belong to Ulas Yilmaz.
%    You are welcome to use it for non-commercial purposes, such as
%    student projects, research and personal interest. However,
%    you are not allowed to use it for commercial purposes, without
%    an explicit written and signed license agreement with Ulas Yilmaz.
%    Berlin University of Technology, Germany 2006.
%    http://www.cv.tu-berlin.de/~ulas/RaRF
%

% number and length of feature vectors
[m a] = size(F1);
[n a] = size(F2);

% ground distance matrix
for i = 1:m
    for j = 1:n
        f(i, j) = Func(F1(i, 1:a), F2(j, 1:a));
    end
end

% gdm in column-vector form
f = f';
f = f(:);

end

function [E] = gdf(V1, V2)
%
% GDF   Ground distance between two vectors
%    [E] = GDF(F1, F2) is the ground distance between two feature vectors.
%
%    Example:
%    -------
%        v1 = [100, 40, 22];
%        v2 = [50, 100, 80];
%        ...
%        [e] = gdf(v1, v2);
%        ...
%
%    This file and its content belong to Ulas Yilmaz.
%    You are welcome to use it for non-commercial purposes, such as
%    student projects, research and personal interest. However,
%    you are not allowed to use it for commercial purposes, without
%    an explicit written and signed license agreement with Ulas Yilmaz.
%    Berlin University of Technology, Germany 2006.
%    http://www.cv.tu-berlin.de/~ulas/RaRF
%

E = norm(V1 - V2, 2);

end