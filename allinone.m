% Main entry
clear;clc;

if ~exist('intel_lab_dataset.mat', 'file')
    labdata = dataset('File', 'data.txt', 'Delimiter',' ');
    labdata.Properties.VarNames{1} = 'date';
    labdata.Properties.VarNames{2} = 'time';
    labdata.Properties.VarNames{3} = 'epoch';
    labdata.Properties.VarNames{4} = 'moteid';
    labdata.Properties.VarNames{5} = 'temperature';
    labdata.Properties.VarNames{6} = 'humidity';
    labdata.Properties.VarNames{7} = 'light';
    labdata.Properties.VarNames{8} = 'voltage';
    save('intel_lab_dataset.mat');
else
    load('intel_lab_dataset.mat');
end

series = initialize(labdata); % or load('series.mat');
test_fmeasure(series);
test_parameter_single(series);
test_parameter(series);
