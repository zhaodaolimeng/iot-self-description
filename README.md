IoT Device Self-Description
=======
This project contains the source code of paper: 'IoT Device Self-Description Method' for CWSN 2014 xi'an.
All the code include is used for simulate the IoT self-description process, which can support the device to create XML descriptions for itself (labels actually :p).
All the plot in the paper uses the computation result of result set 'allinone.mat'.
The optimization method using LP is too sensitive for the anormal-points, commits to fix this problem or any other are welcome.
Any question, please contact zhaodaolimeng@gmail.com.

Qucik Start
-----------
1. Open MATLAB, and set path this project.
2. Download [Intel Lab Data](http://db.csail.mit.edu/labdata/data.txt.gz), and extract to the working directory.
3. Run command: allinone;

Content
-----------
Main simulation process:
* Initialize dataset 
* Select a subset and then optimize the metric for clustering
* Run distribute dbscan method

Files:
* allinone.m						Main entry of the simulation and result plot process.
* initialize.m						Create time series as the input data.
* distribute_optimization.m			Simulate the distribute optimization and distribute clustering.
* distribute_parameter_selection.m	Set parameter for optimization process.
* distribute_dbscan.m				Implementation of a distributed dbscan method.
* feature_distance.m				Compute the distance between any 2 nodes using different metric.
* fmeasure.m						Compute the F-measure of different clustering method for accuracy comparison.
* IoTNodes.m						A class data structure to simulate a IoT device.
* test_fmeasure.m					Create benchmark of different clustering method
* test_parameter.m					Show the accuracy of parameter choosing
* test_parameter_single.m			Virtual show of optimal point in parameter choosing
