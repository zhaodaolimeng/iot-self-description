function [dist, eps, minpts] = distributed_parameter_selection(emd_dist, fft_dist, diff_dist, opt_lambda, minv, realnode)
%% Helper function to compute parameter for clustering

dist = opt_lambda(1)*emd_dist + opt_lambda(2)*fft_dist + opt_lambda(3)*diff_dist;
%!!!!!!!!!!!! PROBLEM !!!!!!!!!!!!!!!
% eps=minv*2/(numel(realnode)); % MAYBE TOO SMALL
% tradeoff=numel(realnode)*minv;
% if tradeoff<1
%    tradeoff=1;
% end
% MinPts=floor(tradeoff); % MAYBE TOO LARGE

eps=minv/2;
minpts=max(1,floor((2*eps)^3*numel(realnode)));    

end