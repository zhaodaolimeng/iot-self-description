function series = initialize(labdata)
% This file is used for initialize the 'series.mat'
moteid=[1,2,3,4,6,7,8,9,10,11, 12,13,14,15,16, 17,18,19,20,21];

for i=1:1:numel(moteid)
    disp(['Compute the time serie for node No.',num2str(moteid(i))])
    thisnode=labdata(labdata.moteid==moteid(i) & strcmp(labdata.date,'2004-03-01'),:);
    thisnode=thisnode(1:100,:);
    thisnode.datetime=cellstr(datestr(strcat(thisnode.date,{' '},thisnode.time), 'yyyy-mm-dd HH:MM:SS'));

    series(i,1)=timeseries(thisnode.temperature,thisnode.datetime);
    series(i,2)=timeseries(thisnode.humidity,thisnode.datetime);
    series(i,3)=timeseries(thisnode.light,thisnode.datetime);
    series(i,4)=timeseries(thisnode.voltage,thisnode.datetime);
end

if ~exist('series.mat', 'file')
    save('series.mat');
end
end


