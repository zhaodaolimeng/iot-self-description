function test_parameter(series)

beta=1;
my_epsilon=0.2:-0.02:0.02;
my_minPts=1:1:5;
max_samples=5;
num_of_loops=20;
moteid=1:1:size(series,1);

resultset_f1=zeros(max_samples,3);
resultset_P=zeros(max_samples,3);
resultset_R=zeros(max_samples,3);

for number_of_sampling=1:1:max_samples
    
    sample=series(randsample(moteid,number_of_sampling),:);
    realnode=[sample; series(randsample(moteid,2*number_of_sampling),:)];    
    oracle=zeros(size(realnode,2)*size(realnode,1),1); 

    % Oracle label, THE TRUTH, shinjitsiwa, itstomohitotsu
    for j=1:1:size(realnode,2)
        for i=1:1:size(realnode,1)
            oracle((j-1)*size(realnode,1)+i)=j;
        end
    end

    % Prepare Dataset
    [opt_dist, emd_dist, fft_dist, diff_dist, opt_v, optL] = features_distance(sample, 1); % compute optimal for comparison
    [zopt, allemd, allfft, alldiff, zopt_v, zoptL]=features_distance(realnode,1);

    % Optimize Parameter
    [opt_lambda, minv] = distributed_optimization(emd_dist, fft_dist, diff_dist, sample);
    [dist, opt_eps, opt_minpts] = distributed_parameter_selection(allemd, allfft, alldiff, opt_lambda, minv, realnode);

    % Cluster Dataset and F-measure
    [message_cnt, labels] = distributed_dbscan(dist, opt_eps, opt_minpts, num_of_loops);
    [ratio(1,1),ratio(1,2),ratio(1,3)] = fmeasure(labels,oracle,beta);   

    best_f=0;
    best_P=0;
    best_R=0;
    
    aver_f=0;
    aver_P=0;
    aver_R=0;
    
    opt_f=ratio(1,1);
    opt_P=ratio(1,2);
    opt_R=ratio(1,3);

    for idx=1:1:numel(my_epsilon)
        epsilon=my_epsilon(idx);
        for minpts=1:1:numel(my_minPts)
            [dist, zeps, zminpts] = distributed_parameter_selection(allemd, allfft, alldiff, opt_lambda, minv, realnode);
            [message_cnt, labels] = distributed_dbscan(dist, epsilon, minpts, num_of_loops);
            [ratio(1,1),ratio(1,2),ratio(1,3)] = fmeasure(labels,oracle,beta);        
            disp(['Eps = ', num2str(epsilon), '  MinPts = ', num2str(minpts)]);
            
            best_f=max(ratio(1,1),best_f);
            best_P=max(ratio(1,2),best_P);
            best_R=max(ratio(1,3),best_R);
                        
            aver_f=aver_f+ratio(1,1);
            aver_P=aver_P+ratio(1,2);
            aver_R=aver_R+ratio(1,3);
        end
    end
    aver_f=aver_f/(numel(my_epsilon)*numel(my_minPts));
    aver_P=aver_P/(numel(my_epsilon)*numel(my_minPts));
    aver_R=aver_R/(numel(my_epsilon)*numel(my_minPts));
    
    resultset_f1(number_of_sampling,1)=best_f;
    resultset_f1(number_of_sampling,2)=aver_f;
    resultset_f1(number_of_sampling,3)=opt_f;        
    resultset_f1(number_of_sampling,:)
    
    resultset_P(number_of_sampling,1)=best_P;
    resultset_P(number_of_sampling,2)=aver_P;
    resultset_P(number_of_sampling,3)=opt_P;
    resultset_P(number_of_sampling,:)
    
    resultset_R(number_of_sampling,1)=best_R;
    resultset_R(number_of_sampling,2)=aver_R;
    resultset_R(number_of_sampling,3)=opt_R;
    resultset_R(number_of_sampling,:)
end
%save('scratch.mat');

range=1:1:max_samples;
% plot(resultset_f1);

figure(6);
plot(range, resultset_P(:,1),'--b*',range,resultset_P(:,2),'-.b*',range, resultset_P(:,3),'--r*',range, resultset_R(:,1),'--bs',range,resultset_R(:,2),'-.bs',range, resultset_R(:,3),'--rs');
axis([range(1) range(end) 0.2 1]);
title('Performance of parameter selection strategy');
xlabel('Number of nodes');
ylabel('%');
legend('Best Precision','Average Precision','Optimized Precision','Best Recall','Average Recall','Optimized Recall','Location','SouthEast');

figure(7);
plot(range, resultset_f1(:,1),'--b*',range,resultset_f1(:,2),'-.b*',range, resultset_f1(:,3),'--r*',range, resultset_R(:,1),'--bs',range,resultset_R(:,2),'-.bs',range, resultset_R(:,3),'--rs');
axis([range(1) range(end) 0.2 1]);
title('Performance of parameter selection strategy');
xlabel('Number of nodes');
ylabel('%');
legend('Best F-measure','Average F-measure','Optimized F-measure','Best Recall','Average Recall','Optimized Recall','Location','SouthEast');


% figure(7);
% plot(range, resultset_R(:,1),'-.b*',range,resultset_R(:,2),':rs',range, resultset_R(:,3),'-.bo');
% axis([range(1) range(end) 0.2 1]);
% title('Accuracy of Optimal Selection');
% xlabel('Number of nodes');
% ylabel('%');
% legend('Best','Average','Gain through optimization','Location','SouthEast');


end

